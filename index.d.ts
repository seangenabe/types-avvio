import { EventEmitter } from 'events'

declare class App<TTopLevel> extends EventEmitter {
  constructor(options?: AvvioOptions, started?: () => void)
  use<TOpts = any>(func: Plugin<this, TOpts>, opts: TOpts)
  after<TOverride = never>(func?: AfterFunc<this, TTopLevel | TOverride>)
  ready<TOverride = never>(func?: AfterFunc<void, TTopLevel | TOverride>)
  start(): void
  override: <TOpts = any, TOverride = any>(
    instance: this,
    plugin: Plugin<TOverride, TOpts>,
    options: TOpts
  ) => TOverride
}

type Plugin<TInstance, TOpts = any> =
  | ((
      func: (instance: TInstance, opts: TOpts, done: () => void) => void,
      opts?: TOpts
    ) => TInstance)
  | ((
      func: (instance: TInstance, opts: TOpts) => Promise<void>,
      opts?: TOpts
    ) => TInstance)

type AfterFunc<TReturn, TContext> = () =>
  | TReturn
  | ((func: (error: Error) => Promise<void>) => TReturn)
  | ((func: (error: Error, done: () => void) => Promise<void>) => TReturn)
  | ((
      func: (error: Error, context: TContext, done: () => void) => void
    ) => TReturn)

interface AvvioOptions {
  expose?: { use?: 'load'; after: 'load'; ready: 'load' }
  autostart?: boolean
}

interface Boot<T = any> {
  (instance: T, options?: AvvioOptions, started?: () => void): T & App<T>
  new (options?: AvvioOptions, started?: () => void): T & App<T>
}

export = App
